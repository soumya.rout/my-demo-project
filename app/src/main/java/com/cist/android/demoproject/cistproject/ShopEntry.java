package com.cist.android.demoproject.cistproject;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class ShopEntry extends AppCompatActivity {
    EditText shop_Name,latitude,longitude;
    private static final int MAP_REQUEST_CODE = 1010;
    Button submit,go_to_product_entry;
    boolean isvalid = false;
    ActionBar actionBar;
    ArrayList<String> arrayList;
    String location = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.shop_entry);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        actionBar = getSupportActionBar();
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Shop Entry");
        shop_Name = (EditText)findViewById(R.id.editText4);
        latitude = (EditText)findViewById(R.id.editText5);
        longitude = (EditText)findViewById(R.id.editText6);
        submit = (Button)findViewById(R.id.button) ;
        go_to_product_entry = (Button)findViewById(R.id.button3);
        arrayList = new ArrayList<String>();
        DatabaseHandler databaseHandler = new DatabaseHandler(ShopEntry.this);
        databaseHandler.getWritableDatabase();
        DatabaseHandler.sqLiteDatabase = ShopEntry.this.openOrCreateDatabase(DatabaseHandler.DATABASE_NAME,MODE_PRIVATE,null);
        go_to_product_entry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ShopEntry.this,Prod_Entry.class);
                startActivity(intent);
                finish();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doValidation();
                if(isvalid){
                    Toast.makeText(ShopEntry.this, "Please fill the fields", Toast.LENGTH_SHORT).show();

                }else {
                    DatabaseHandler.cursor = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE,null);
                    if(DatabaseHandler.cursor.getCount()>0){
                        DatabaseHandler.cursor.moveToFirst();
                        do{
                            String shopName = DatabaseHandler.cursor.getString(1);
                            arrayList.add(shopName);
                        }while (DatabaseHandler.cursor.moveToNext());

                    }
                    if(arrayList.contains(shop_Name.getText().toString().trim())){
                        Toast.makeText(ShopEntry.this, "This shop is already exists.", Toast.LENGTH_SHORT).show();

                    }else {

                        ContentValues contentValues = new ContentValues();
                        contentValues.put("Shop_Name", shop_Name.getText().toString());
                        contentValues.put("Lat", latitude.getText().toString());
                        contentValues.put("Long", longitude.getText().toString());
                        contentValues.put("Place_Name", location);

                        long s = DatabaseHandler.sqLiteDatabase.insert(DatabaseHandler.SHOP_TABLE, null, contentValues);
                        Toast.makeText(ShopEntry.this, "Data submitted successfully.", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ShopEntry.this, Prod_Entry.class);
                        startActivity(intent);
                        finish();
                    }
                }
            }
        });
        latitude.setOnClickListener(
                new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ShopEntry.this,MapActivity.class);
                startActivityForResult(intent, MAP_REQUEST_CODE);

            }
        });

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.v("CIST", "requestCode:" + requestCode + ", resultCode:" + resultCode + ", data:" + data);


                if (resultCode == 1001) {
                    if (data != null) {
                        latitude.setText(data.getDoubleExtra("lat", 0) + "");
                        longitude.setText(data.getDoubleExtra("lag", 0) + "");
                        location = data.getStringExtra("Location");
                    }
                }


        super.onActivityResult(requestCode, resultCode, data);
    }
    public void doValidation(){
        isvalid = false;
        if(shop_Name.getText().toString().equals("")||latitude.getText().toString().equals("")
                ||longitude.getText().toString().equals("")){
            isvalid = true;
            Toast.makeText(this, "Please fill the fields", Toast.LENGTH_SHORT).show();
        }else{
//            if(shop_Name.getText().toString().equals("")){
//                isvalid = true;
//                Toast.makeText(this, "Please give shop name", Toast.LENGTH_SHORT).show();
//
//            }
//            if(latitude.getText().toString().equals("")){
//                isvalid = true;
//                Toast.makeText(this, "Please give latitude name", Toast.LENGTH_SHORT).show();
//
//            }
//            if(longitude.getText().toString().equals("")){
//                isvalid = true;
//                Toast.makeText(this, "Please give longitude name", Toast.LENGTH_SHORT).show();
//
//            }


        }

    }
    @Override
    public  void onBackPressed(){
        Intent intent = new Intent(ShopEntry.this,UserActivity.class);
        startActivity(intent);
        finish();
    }
}
