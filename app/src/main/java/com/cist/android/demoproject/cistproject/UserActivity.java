package com.cist.android.demoproject.cistproject;

import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class UserActivity extends AppCompatActivity {
    ConstraintLayout cnst_shop,cnst_prdct;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        actionBar = getSupportActionBar();
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("User Select");
        cnst_shop = (ConstraintLayout)findViewById(R.id.constraint);
        cnst_prdct = (ConstraintLayout)findViewById(R.id.constraint1);
        cnst_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserActivity.this,ShopEntry.class);
                startActivity(intent);
                finish();
            }
        });
        cnst_prdct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(UserActivity.this,ProductSearch.class);
                startActivity(intent);
                finish();
            }
        });


    }
}
