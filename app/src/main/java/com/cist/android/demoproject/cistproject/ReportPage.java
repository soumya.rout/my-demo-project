package com.cist.android.demoproject.cistproject;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class ReportPage extends AppCompatActivity {
    ListView listview;
    ArrayList<String> shopName;
    ArrayList<String> prodName;
    ArrayList<String> cost;
    DatabaseHandler databaseHandler;
    String shop_name = "";
    String shop_id = "";
    Button findDist,add_new_shop;
    ActionBar actionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_page);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        actionBar = getSupportActionBar();
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Report");
        listview = (ListView)findViewById(R.id.listview);
        findDist = (Button)findViewById(R.id.button5);
        add_new_shop = (Button)findViewById(R.id.button6);
        shopName = new ArrayList<String>();
        prodName = new ArrayList<String>();
        cost = new ArrayList<String>();
        databaseHandler =new DatabaseHandler(ReportPage.this);
        databaseHandler.getWritableDatabase();
        DatabaseHandler.sqLiteDatabase = ReportPage.this.openOrCreateDatabase(DatabaseHandler.DATABASE_NAME,MODE_PRIVATE,null);
        getData();
        add_new_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ReportPage.this,ShopEntry.class);
                startActivity(intent);
                finish();
            }
        });
        findDist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Cursor cursor1 = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE , null);
                if(cursor1.getCount()>1) {
                    Intent intent = new Intent(ReportPage.this,DistanceActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    Toast.makeText(ReportPage.this, "Please Add atleast one more shop to find distance between them", Toast.LENGTH_SHORT).show();
                }


            }
        });




    }
    @Override
    public  void onBackPressed(){
        Intent intent = new Intent(ReportPage.this,Prod_Entry.class);
        startActivity(intent);
        finish();
    }
    public void getData(){
        Cursor cursor1 = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE , null);
        if(cursor1.getCount()>0){
            shopName.clear();
            prodName.clear();
            cost.clear();
            cursor1.moveToFirst();
            do{
                shop_name= cursor1.getString(1);
                shop_id = cursor1.getString(0);
                DatabaseHandler.cursor =DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.PRODUCT_TABLE + " WHERE Shop_id = '" + shop_id + "'", null);
                if(DatabaseHandler.cursor.getCount()>0){
                    DatabaseHandler.cursor.moveToFirst();
                    do{
                        String Shop_Id = DatabaseHandler.cursor.getString(1);
                        String Product_Name= DatabaseHandler.cursor.getString(2);
                        String Cost = DatabaseHandler.cursor.getString(3);
                        shopName.add(shop_name);
                        prodName.add(Product_Name);
                        cost.add(Cost);
                    }while(DatabaseHandler.cursor.moveToNext());
                }
            }while (cursor1.moveToNext());
            MyBaseAdapter myBaseAdapter = new MyBaseAdapter(this,shopName,prodName,cost);
            listview.setAdapter(myBaseAdapter);

        }


    }
    public class MyBaseAdapter extends BaseAdapter{

        ArrayList<String> shopName;
        ArrayList<String> prodName;
        ArrayList<String> cost;
        Context context;
        public MyBaseAdapter(Context context, ArrayList<String> shopName, ArrayList<String> prodName, ArrayList<String> cost) {
            this.shopName = shopName;
            this.prodName =prodName;
            this.cost = cost;
            this.context = context;
        }

        @Override
        public int getCount() {
            return shopName.size();
        }

        @Override
        public Object getItem(int i) {
            return shopName.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            view = layoutInflater.inflate(R.layout.report,viewGroup,false);
            final TextView shopname = (TextView)view.findViewById(R.id.textView2);
            final TextView prodname = (TextView)view.findViewById(R.id.textView5);
            final TextView Cost = (TextView)view.findViewById(R.id.textView6);
            final Button findDistance =(Button)view.findViewById(R.id.textView10) ;
            findDistance.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ReportPage.this,DistanceActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            shopname.setText(shopName.get(i));
            prodname.setText(prodName.get(i));
            Cost.setText(cost.get(i));


            return view;
        }
    }


}

