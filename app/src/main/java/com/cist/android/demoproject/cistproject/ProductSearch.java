package com.cist.android.demoproject.cistproject;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ProductSearch extends AppCompatActivity {
    Spinner spinner;
    DatabaseHandler databaseHandler;
    ArrayList<String> arrayList;
    ArrayAdapter<String> arrayAdapter;
    ActionBar actionBar;
    Button searchProduct;
    ArrayList<String> ShopName_list;
    ArrayList<String> locationList;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_search);
        spinner = (Spinner)findViewById(R.id.spinner);
        searchProduct = (Button) findViewById(R.id.button7);
        listView = (ListView) findViewById(R.id.listView);
        databaseHandler = new DatabaseHandler(this);
        databaseHandler.getWritableDatabase();
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        actionBar = getSupportActionBar();
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Search Product");
        arrayList = new ArrayList<String>();
        ShopName_list = new ArrayList<String>();
        locationList = new ArrayList<String>();
        arrayList.add("Select");
        searchProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String productName = (String) spinner.getSelectedItem();
                DatabaseHandler.cursor = DatabaseHandler.sqLiteDatabase.rawQuery("SELECT * FROM " +DatabaseHandler.PRODUCT_TABLE + " WHERE Product_Name = '" + productName + "'",null);
                if(DatabaseHandler.cursor.getCount()>0) {
                    DatabaseHandler.cursor.moveToFirst();
                    do {
                        String id = DatabaseHandler.cursor.getString(1);
                        Cursor cursor = DatabaseHandler.sqLiteDatabase.rawQuery("SELECT * FROM " + DatabaseHandler.SHOP_TABLE + " WHERE Id = '" + id + "'", null);
                        if (cursor.getCount() > 0) {
                            cursor.moveToFirst();
                            do {
                                String ShopName = cursor.getString(1);
                                String Location = cursor.getString(4);
                                ShopName_list.add(ShopName);
                                locationList.add(Location);


                            } while (cursor.moveToNext());
                        }
                    }
                    while (DatabaseHandler.cursor.moveToNext());

                }
                MyListAdapter myListAdapter = new MyListAdapter(ProductSearch.this,ShopName_list,locationList);
                listView.setAdapter(myListAdapter);
            }
        });
        DatabaseHandler.sqLiteDatabase = ProductSearch.this.openOrCreateDatabase(DatabaseHandler.DATABASE_NAME,MODE_PRIVATE,null);
        DatabaseHandler.cursor = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.PRODUCT_TABLE , null);
        if(DatabaseHandler.cursor.getCount()>0){
            DatabaseHandler.cursor.moveToFirst();
            do {
                String product_name = DatabaseHandler.cursor.getString(2);
                arrayList.add(product_name);
            }while (DatabaseHandler.cursor.moveToNext());
        }
        arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,arrayList);
        spinner.setAdapter(arrayAdapter);

    }
    public class MyListAdapter extends BaseAdapter {

        ArrayList<String> locationList;
        ArrayList<String> shopList;
        Context context;
        public MyListAdapter(Context context, ArrayList<String> shopList, ArrayList<String> locationList) {
            this.locationList = locationList;
            this.shopList =shopList;
            this.context = context;
        }

        @Override
        public int getCount() {
            return shopList.size();
        }

        @Override
        public Object getItem(int i) {
            return shopList.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            view = layoutInflater.inflate(R.layout.list_search,viewGroup,false);
            final TextView shopname = (TextView)view.findViewById(R.id.textView17);
            final TextView location = (TextView)view.findViewById(R.id.textView18);

            shopname.setText(shopList.get(i));
            location.setText(locationList.get(i));


            return view;
        }
    }
    @Override
    public  void onBackPressed(){
        Intent intent = new Intent(ProductSearch.this,UserActivity.class);
        startActivity(intent);
        finish();
    }
}
