package com.cist.android.demoproject.cistproject;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHandler extends SQLiteOpenHelper {
    public static Cursor cursor;
    public static SQLiteDatabase sqLiteDatabase;
    public static String SHOP_TABLE = "shop_table";
    public static String PRODUCT_TABLE = "product_table";
    public static String DATABASE_NAME = "CIST_sqlite";
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseHandler.SHOP_TABLE + "(Id INTEGER PRIMARY KEY AUTOINCREMENT,Shop_Name TEXT,Lat TEXT,Long TEXT,Place_Name TEXT);");
        sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + DatabaseHandler.PRODUCT_TABLE + "(Id INTEGER PRIMARY KEY AUTOINCREMENT,Shop_id INTEGER,Product_Name TEXT,Cost TEXT);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
