package com.cist.android.demoproject.cistproject;

import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DistanceActivity extends AppCompatActivity {
    Spinner shopName1,shopName2;
    Button find,add_new_shop;
    DatabaseHandler databaseHandler;
    String Lat_shop1 = "",Lng_shop1 = "",Lat_shop2 = "",Lng_shop2 = "";
    ArrayList arrayList_shop1 = new ArrayList();
    ArrayList arrayList_shop2 = new ArrayList();
    String shop_name1 = "",shop_name2 = "";
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.distance);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        actionBar = getSupportActionBar();
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Distance");
        shopName1 = (Spinner) findViewById(R.id.editText);
        shopName2 = (Spinner)findViewById(R.id.editText2);
        find = (Button) findViewById(R.id.button4);
        add_new_shop = (Button) findViewById(R.id.button4);
        arrayList_shop1.add("Select");
        arrayList_shop2.add("Select");
        getdata();
        ArrayAdapter<String> spinnerArrayAdapter1 = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, arrayList_shop1); //selected item will look like a spinner set from XML
        shopName1.setAdapter(spinnerArrayAdapter1);
       final ArrayAdapter<String> spinnerArrayAdapter2 = new ArrayAdapter<String>
                (this, android.R.layout.simple_spinner_item, arrayList_shop2); //selected item will look like a spinner set from XML
        shopName2.setAdapter(spinnerArrayAdapter2);
        add_new_shop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DistanceActivity.this,ShopEntry.class);
                startActivity(intent);
                finish();
            }
        });

        find.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseHandler = new DatabaseHandler(DistanceActivity.this);
                databaseHandler.getWritableDatabase();
                DatabaseHandler.sqLiteDatabase = DistanceActivity.this.openOrCreateDatabase(DatabaseHandler.DATABASE_NAME, MODE_PRIVATE, null);
                Cursor cursor1 = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE + " WHERE Shop_Name = '" + shop_name1 + "'", null);
                if (cursor1.getCount() > 0) {

                    cursor1.moveToFirst();

                    Lat_shop1 = cursor1.getString(2);
                    Lng_shop1 = cursor1.getString(3);


                }
                 Cursor cursor2 = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE + " WHERE Shop_Name = '" + shop_name2 + "'", null);
                if (cursor2.getCount() > 0) {

                    cursor2.moveToFirst();

                    Lat_shop2 = cursor2.getString(2);
                    Lng_shop2 = cursor2.getString(3);


                }
                getKmFromLatLong(Float.parseFloat(Lat_shop1),Float.parseFloat(Lng_shop1),Float.parseFloat(Lat_shop2),Float.parseFloat(Lng_shop2));

            }
        });
        shopName1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               if (!adapterView.getSelectedItem().toString().equals("Select")){
                   shop_name1 = arrayList_shop1.get(i).toString();
                   arrayList_shop2.clear();
                   arrayList_shop2.add("Select");
                   DatabaseHandler.cursor = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE + " GROUP BY Shop_Name",null);
                   if(DatabaseHandler.cursor.getCount()>0){
                       DatabaseHandler.cursor.moveToFirst();
                       do {
                           String ShopName = DatabaseHandler.cursor.getString(1);
                           arrayList_shop2.add(ShopName);

                       }while (DatabaseHandler.cursor.moveToNext());

                   }
                   arrayList_shop2.remove(shop_name1);
                   spinnerArrayAdapter2.notifyDataSetChanged();

               }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        shopName2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (!adapterView.getSelectedItem().toString().equals("")){
                    shop_name2 = arrayList_shop2.get(i).toString();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    public float getKmFromLatLong(float lat1, float lng1, float lat2, float lng2){
        Location loc1 = new Location("");
        loc1.setLatitude(lat1);
        loc1.setLongitude(lng1);
        Location loc2 = new Location("");
        loc2.setLatitude(lat2);
        loc2.setLongitude(lng2);
        float distanceInMeters = loc1.distanceTo(loc2);
        Toast.makeText(DistanceActivity.this,"Distance = "+""+ distanceInMeters/1000+"Km", Toast.LENGTH_LONG).show();
        return distanceInMeters/1000;

    }
    public void getdata(){
        Cursor cursor = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT Shop_Name FROM " + DatabaseHandler.SHOP_TABLE + " GROUP BY Shop_Name ", null);
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            do {
                arrayList_shop2.add(cursor.getString(0));
                arrayList_shop1.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
    }
    @Override
    public  void onBackPressed(){
        Intent intent = new Intent(DistanceActivity.this,ReportPage.class);
        startActivity(intent);
        finish();
    }
}
