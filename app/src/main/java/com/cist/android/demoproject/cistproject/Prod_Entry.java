package com.cist.android.demoproject.cistproject;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.ArrayList;

public class Prod_Entry extends AppCompatActivity {
    AutoCompleteTextView autoCompleteTextView;
    EditText productName,cost;
    Button submit;
    ArrayList arrayList;
    ArrayAdapter arrayAdapter;
    ImageButton imageButton;
    Button Submit;
    String ShopId ="";
    ActionBar actionBar;
    boolean isvalid = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.prod__entry);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        actionBar = getSupportActionBar();
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("Product Entry");

        autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView);
        productName = (EditText)findViewById(R.id.product);
        cost = (EditText)findViewById(R.id.cost);
        submit = (Button) findViewById(R.id.button2);
        imageButton = (ImageButton)findViewById(R.id.imageButton) ;
        Submit= (Button)findViewById(R.id.button2);
        arrayList = new ArrayList();
        DatabaseHandler databaseHandler = new DatabaseHandler(Prod_Entry.this);
        databaseHandler.getWritableDatabase();
        DatabaseHandler.sqLiteDatabase = Prod_Entry.this.openOrCreateDatabase(DatabaseHandler.DATABASE_NAME,MODE_PRIVATE,null);
        DatabaseHandler.cursor = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE , null);
        if(DatabaseHandler.cursor.getCount()>0){
            DatabaseHandler.cursor.moveToFirst();
            do {
                String ShopName = DatabaseHandler.cursor.getString(1);
                String lat = DatabaseHandler.cursor.getString(2);
                String lng = DatabaseHandler.cursor.getString(3);
                arrayList.add(ShopName);
            }while (DatabaseHandler.cursor.moveToNext());


        }
        arrayAdapter = new ArrayAdapter(this,R.layout.prod,arrayList);
        autoCompleteTextView.setAdapter(arrayAdapter);
        autoCompleteTextView.setThreshold(2);
        Submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                doValidation();
                if (isvalid) {
                    Toast.makeText(Prod_Entry.this, "Please fill the fields", Toast.LENGTH_SHORT).show();

                } else {
                    DatabaseHandler.cursor = DatabaseHandler.sqLiteDatabase.rawQuery(" SELECT * FROM " + DatabaseHandler.SHOP_TABLE + " WHERE Shop_Name = '" + autoCompleteTextView.getText().toString() + "'", null);
                    if (DatabaseHandler.cursor.getCount() > 0) {
                        DatabaseHandler.cursor.moveToFirst();
                        ShopId = DatabaseHandler.cursor.getString(0);
                    }

                    ContentValues contentValues = new ContentValues();
                    contentValues.put("Shop_id", ShopId);

                    contentValues.put("Product_Name", productName.getText().toString());
                    contentValues.put("Cost", cost.getText().toString());
                    long l = DatabaseHandler.sqLiteDatabase.insert(DatabaseHandler.PRODUCT_TABLE, null, contentValues);
                    Intent intent = new Intent(Prod_Entry.this, ReportPage.class);
                    startActivity(intent);
                    finish();


                }
            }

        });

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                autoCompleteTextView.showDropDown();
            }
        });

    }
    @Override
    public  void onBackPressed(){
        Intent intent = new Intent(Prod_Entry.this,ShopEntry.class);
        startActivity(intent);
        finish();
    }
    public void doValidation(){
        isvalid = false;
        if(productName.getText().toString().equals("")||autoCompleteTextView.getText().toString().equals("")
                ||cost.getText().toString().equals("")){
            isvalid = true;
            Toast.makeText(this, "Please fill the fields", Toast.LENGTH_SHORT).show();
        }else{
//            if(shop_Name.getText().toString().equals("")){
//                isvalid = true;
//                Toast.makeText(this, "Please give shop name", Toast.LENGTH_SHORT).show();
//
//            }
//            if(latitude.getText().toString().equals("")){
//                isvalid = true;
//                Toast.makeText(this, "Please give latitude name", Toast.LENGTH_SHORT).show();
//
//            }
//            if(longitude.getText().toString().equals("")){
//                isvalid = true;
//                Toast.makeText(this, "Please give longitude name", Toast.LENGTH_SHORT).show();
//
//            }


        }

    }

}
